﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Test
{
    public class ItemTest
    {
        /// <summary>
        /// Quando o (PrazoParaVenda) do item tiver passado, a (Qualidade) do item diminui duas vezes mais rápido.
        /// </summary>

        [Fact]
        public void ITEM_FORA_DO_PRAZO_PERDE_QUALIDADE_DUAS_VEZES_MAIS()
        {
            var item = new Item("Teste", 0, 10);

            item.VerificarQualidade();

            Assert.Equal(8, item.Qualidade);
        }

        /// <summary>
        /// A (Qualidade) do item não pode ser negativa
        /// </summary>
        [Fact]
        public void QUALIDADE_DO_ITEM_NAO_PODE_SER_NEGATIVA()
        {
            const int prazo = 2;
            const int qualidade = 1;

            var item = new Item("Teste", prazo, qualidade);

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.Equal(0, item.Qualidade);
        }

        /// <summary>
        /// A (Qualidade) de um item não pode ser maior que 50.
        /// </summary>
        [Fact]
        public void QUALIDADE_DO_ITEM_NAO_PODE_SER_MAIOR_QUE_50()
        {
            var item = new Item("Teste", 10, 150);

            Assert.Equal(50, item.Qualidade);
        }
    }
}