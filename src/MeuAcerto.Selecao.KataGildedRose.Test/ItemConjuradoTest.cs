﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Test
{
    public class ItemConjuradoTest
    {
        /// <summary>
        /// Os itens "Conjurados" (Conjurado) diminuem a (Qualidade) duas vezes mais rápido que os outros itens.
        /// </summary>
        [Fact]
        public void PERDE_QUALIDADE_DUAS_VEZES_MAIS_RAPIDO()
        {
            var prazo = 5;
            var qualidade = 18;

            var item = new ItemConjurado("Teste", prazo, qualidade);

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            var expected = qualidade - (prazo * 2);

            Assert.Equal(expected, item.Qualidade);
        }
    }
}