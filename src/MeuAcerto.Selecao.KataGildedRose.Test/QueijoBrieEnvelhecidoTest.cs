﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Test
{
    public class QueijoBrieEnvelhecidoTest
    {
        /// <summary>
        /// O (Queijo Brie envelhecido), aumenta sua qualidade (Qualidade) em 1 unidade a medida que envelhece.
        /// </summary>
        [Fact]
        public void AUMENTA_QUALIDADE_QUANDO_PRAZO_DIMINUI()
        {
            const int prazo = 5;
            const int qualidade = 1;

            var item = new QueijoBrieEnvelhecido(prazo, qualidade);

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.Equal(prazo + qualidade, item.Qualidade);
        }
    }
}