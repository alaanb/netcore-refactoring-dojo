﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Test
{
    public class DenteDoTarrasqueTest
    {
        /// <summary>
        /// O item (Dente do Tarrasque), por ser um item lendário, não precisa ter um (PrazoParaVenda) e sua (Qualidade) não precisa ser diminuída.
        /// </summary>
        [Fact]
        public void QUALIDADE_NAO_PODE_SER_DIMINUIDA()
        {
            var item = new DenteDoTarrasque();

            var prazo = 10;
            var qualidade = item.Qualidade;

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.Equal(qualidade, item.Qualidade);
        }

        /// <summary>
        /// O (Dente do Tarrasque) por ser um item lendário vai ter uma qualidade imutável de 80.
        /// </summary>
        [Fact]
        public void QUALIDADE_DEVE_SER_SEMPRE_80()
        {
            var prazo = 15;

            var item = new DenteDoTarrasque();

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.Equal(80, item.Qualidade);
        }
    }
}