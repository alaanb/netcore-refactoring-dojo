﻿using MeuAcerto.Selecao.KataGildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Test
{
    public class IngressoTest
    {
        /// <summary>
        /// O item (Ingressos) aumenta sua (Qualidade) a medida que o (PrazoParaVenda) se aproxima
        /// </summary>
        [Fact]
        public void QUALIDADE_NAO_DIMINUI()
        {
            var prazo = 15;
            var qualidade = 1;

            var item = new Ingresso("Teste", prazo, qualidade);

            for (int i = 0; i < prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.True(item.Qualidade > qualidade);
        }

        /// <summary>
        /// A (Qualidade) aumenta em 2 unidades quando o (PrazoParaVenda) é igual ou menor que 10.
        /// </summary>
        [Fact]
        public void QUALIDADE_AUMENTA_QUANDO_PRAZO_MENOR_QUE_10()
        {
            var quantidadeDeAumento = 2;
            var prazo = 8;
            var qualidade = 0;
            var quantidadeDeDiasParaVerificacao = 2;

            var item = new Ingresso("Teste", prazo, qualidade);

            for (int i = 0; i < quantidadeDeDiasParaVerificacao; i++)
            {
                item.VerificarQualidade();
            }

            var expected = (quantidadeDeDiasParaVerificacao * quantidadeDeAumento) + qualidade;

            Assert.Equal(expected, item.Qualidade);
        }

        /// <summary>
        /// A (Qualidade) aumenta em 2 unidades quando o (PrazoParaVenda) é igual ou menor que 5.
        /// </summary>
        [Fact]
        public void QUALIDADE_AUMENTA_QUANDO_PRAZO_MENOR_QUE_5()
        {
            var quantidadeDeAumento = 3;
            var prazo = 4;
            var qualidade = 0;
            var quantidadeDeDiasParaVerificacao = 2;

            var item = new Ingresso("Teste", prazo, qualidade);

            for (int i = 0; i < quantidadeDeDiasParaVerificacao; i++)
            {
                item.VerificarQualidade();
            }

            var expected = (quantidadeDeDiasParaVerificacao * quantidadeDeAumento) + qualidade;

            Assert.Equal(expected, item.Qualidade);
        }

        /// <summary>
        /// A (Qualidade) do item vai direto à 0 quando o (PrazoParaVenda) tiver passado.
        /// </summary>
        [Fact]
        public void QUALIDADE_ZERA_QUANDO_PRAZO_ACABA()
        {
            var prazo = 4;
            var qualidade = 10;

            var item = new Ingresso("Teste", prazo, qualidade);

            for (int i = 0; i <= prazo; i++)
            {
                item.VerificarQualidade();
            }

            Assert.Equal(0, item.Qualidade);
        }
    }
}