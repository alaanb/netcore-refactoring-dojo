﻿using ConsoleTables;
using MeuAcerto.Selecao.KataGildedRose.Domain;
using MeuAcerto.Selecao.KataGildedRose.Domain.Services;
using System;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;

            var itens = new List<Item>();

            itens.Add(new Item("Corselete +5 DEX", 10, 20));
            itens.Add(new QueijoBrieEnvelhecido(2, 0));
            itens.Add(new Item("Elixir do Mangusto", 5, 7));
            itens.Add(new DenteDoTarrasque());
            itens.Add(new DenteDoTarrasque());
            itens.Add(new Ingresso("Ingressos para o concerto do Turisas", 15, 20));
            itens.Add(new Ingresso("Ingressos para o concerto do Turisas", 10, 49));
            itens.Add(new Ingresso("Ingressos para o concerto do Turisas", 5, 49));
            itens.Add(new ItemConjurado("Bolo de Mana Conjurado", 3, 6));

            var app = new GildedRose(itens);

            for (var i = 0; i < 31; i++)
            {
                Console.WriteLine($"Dia {i}");

                var tabela = new ConsoleTable("Nome", "Prazo para venda", "Qualidade do Item");

                for (var j = 0; j < itens.Count; j++)
                {
                    tabela.AddRow(itens[j].Nome, itens[j].PrazoParaVenda, itens[j].Qualidade);
                }

                tabela.Write(Format.Alternative);

                Console.WriteLine();

                app.AtualizarQualidade();
            }
        }
    }
}