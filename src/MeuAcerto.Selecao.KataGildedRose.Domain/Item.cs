﻿namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class Item
    {
        private int qualidade;

        public Item(string nome, int prazo, int qualidade)
        {
            if (qualidade > QualidadeMaxima)
            {
                qualidade = QualidadeMaxima;
            }

            this.Nome = nome;
            this.PrazoParaVenda = prazo;
            this.Qualidade = qualidade;
        }

        public string Nome { get; protected set; }

        public int PrazoParaVenda { get; protected set; }

        public int Qualidade
        {
            get
            {
                return qualidade;
            }
            set
            {
                if (value >= 0)
                {
                    qualidade = value;
                }
            }
        }

        /// <summary>
        /// Qualidade máxima de um item
        /// </summary>
        protected virtual int QualidadeMaxima
        {
            get
            {
                return 50;
            }
        }

        public virtual void VerificarQualidade()
        {
            if (PrazoParaVenda > 0)
            {
                Qualidade -= 1;
            }
            else
            {
                Qualidade -= 2;
            }

            PrazoParaVenda--;
        }
    }
}
