﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class ItemConjurado : Item
    {
        public ItemConjurado(string nome, int prazo, int qualidade) : base(nome, prazo, qualidade)
        {
        }

        public override void VerificarQualidade()
        {
            Qualidade -= 2;
            PrazoParaVenda--;
        }
    }
}