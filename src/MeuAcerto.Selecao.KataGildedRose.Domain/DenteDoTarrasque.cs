﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class DenteDoTarrasque : Item
    {
        private const int QUALIDADE_MAXIMA = 80;

        public DenteDoTarrasque() : base("Dente do Tarrasque", 0, QUALIDADE_MAXIMA)
        {
        }

        public override void VerificarQualidade()
        {
            // O item (Dente do Tarrasque), por ser um item lendário, não precisa ter um (PrazoParaVenda) e sua (Qualidade) não precisa ser diminuída.
        }

        protected override int QualidadeMaxima => QUALIDADE_MAXIMA;
    }
}