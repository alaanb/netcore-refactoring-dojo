﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose.Domain.Services
{
    public class GildedRose
    {
        private readonly IList<Item> itens;

        public GildedRose(IList<Item> itens)
        {
            this.itens = itens;
        }

        public void AtualizarQualidade()
        {
            foreach (var item in itens)
            {
                item.VerificarQualidade();
            }
        }
    }
}