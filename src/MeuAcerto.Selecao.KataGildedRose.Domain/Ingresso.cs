﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class Ingresso : Item
    {
        public Ingresso(string nome, int prazo, int qualidade) : base(nome, prazo, qualidade)
        {
        }

        public override void VerificarQualidade()
        {
            if (PrazoParaVenda <= 0)
            {
                Qualidade = 0;
            }
            else
            {
                if (PrazoParaVenda > 5 && PrazoParaVenda <= 10)
                {
                    Qualidade += 2;
                }
                else if (PrazoParaVenda <= 5)
                {
                    Qualidade += 3;
                }
                else
                {
                    Qualidade += 1;
                }
            }

            PrazoParaVenda--;
        }
    }
}