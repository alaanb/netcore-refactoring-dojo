﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Domain
{
    public class QueijoBrieEnvelhecido : Item
    {
        public QueijoBrieEnvelhecido(int prazo, int qualidade) : base("Queijo Brie envelhecido", prazo, qualidade)
        {
        }

        public override void VerificarQualidade()
        {
            Qualidade++;
            PrazoParaVenda--;
        }
    }
}